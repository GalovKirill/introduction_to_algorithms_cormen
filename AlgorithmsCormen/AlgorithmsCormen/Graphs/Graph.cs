using System.Collections.Generic;
using System.Linq;

namespace AlgorithmsCormen.Graphs
{
    public abstract class Graph
    {
        public abstract IEnumerable<Vertex> this[Vertex v] { get; }
        public abstract IEnumerable<Vertex> Vertices { get; }

        /// <summary>
        /// Поиск в ширину
        /// </summary>
        /// <param name="start"></param>
        public void BreadthFirstSearch(Vertex start)
        {
            if (!Vertices.Contains(start)) return;

            foreach (var vertex in Vertices.Where(v => start != v))
            {
                vertex.VertexColor = Vertex.Color.White;
                vertex.D = int.MaxValue;
                vertex.Prev = null;
            }

            start.VertexColor = Vertex.Color.Gray;
            start.D = 0;
            start.Prev = null;

            var queue = new Queue<Vertex>(Vertices.Count() / 2);
            queue.Enqueue(start);

            while (queue.Count != 0)
            {
                var u = queue.Dequeue();
                foreach (var vertex in this[u].Where(v => v.VertexColor == Vertex.Color.White))
                {
                    vertex.VertexColor = Vertex.Color.Gray;
                    vertex.D = u.D + 1;
                    vertex.Prev = u;
                    queue.Enqueue(vertex);
                }

                u.VertexColor = Vertex.Color.Black;
            }
        }

        public void DFS()
        {
            foreach (var vertex in Vertices)
            {
                vertex.VertexColor = Vertex.Color.White;
                vertex.Prev = null;
            }

            int time = 0;

            foreach (var vertex in Vertices)
            {
                if (vertex.VertexColor == Vertex.Color.White)
                {
                    DFSVisit(vertex);
                }
            }

            void DFSVisit(Vertex u)
            {
                u.D = ++time;
                u.VertexColor = Vertex.Color.Gray;
                foreach (var vertex in this[u])
                {
                    if (vertex.VertexColor != Vertex.Color.White) continue;
                    vertex.Prev = u;
                    DFSVisit(vertex);
                }
                u.VertexColor = Vertex.Color.Black;
                u.F = ++time;
            }
        }
        
        public class Vertex
        {
            public enum Color
            {
                White,
                Gray,
                Black
            }

            public Color VertexColor { get; set; }
            public int D { get; set; }
            public int F { get; set; }
            public Vertex Prev { get; set; }
        }

        public class Edge
        {
            public Vertex V { get; set; }
            public Vertex U { get; set; }
            public int F { get; set; }
        }
    }
}