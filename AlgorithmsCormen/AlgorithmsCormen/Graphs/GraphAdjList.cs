using System;
using System.Collections.Generic;
using System.Linq;

namespace AlgorithmsCormen.Graphs
{
    public class GraphAdjList : Graph
    {
        public IDictionary<Vertex, IEnumerable<Edge>> VerticesAssociative { get; } =
            new Dictionary<Vertex, IEnumerable<Edge>>();


        public override IEnumerable<Vertex> this[Vertex v] => VerticesAssociative[v].Select(e => e.U);
        
        public override IEnumerable<Vertex> Vertices => VerticesAssociative.Keys;

        
    }
}