--import Text.Layout.Table

import Data.List
data TimeSelection = Milliseconds | Seconds | Minutes | Hours | Days | Month | Years | Centry
    deriving (Show, Read, Enum, Eq) 



f = head $ dropWhile (\n ->(getTime (log n)) /= Seconds) [1..]
	
getTime t = getTime' t Seconds where
    getTime' _ Centry = Centry
    getTime' t s = if isTimeSelection t s then s else getTime' t (succ s)

toMillisecond :: Num a => TimeSelection -> a
toMillisecond Milliseconds = 1000
toMillisecond Seconds = 60 * 1000
toMillisecond Minutes = 60 * toMillisecond Seconds
toMillisecond Hours = 60 * toMillisecond Minutes
toMillisecond Days = 24 * toMillisecond Hours
toMillisecond Month = 30 * toMillisecond Days
toMillisecond Years = 12 * toMillisecond Month
toMillisecond Centry = 100 * toMillisecond Years

isTimeSelection :: (Ord a, Num a) => a -> TimeSelection -> Bool
isTimeSelection t Centry = t >= toMillisecond Centry
isTimeSelection t s = t >= toMillisecond s && t <= toMillisecond (succ s)


--type Func = ((Num a => a -> a), String)

lgn = (log, "lg n")



main :: IO()
main = print "hello"