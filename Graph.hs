
data Color = White | Grey | Black

data Vertex = Vertex { getD :: Int, getColor :: Color, getPrev :: Maybe Vertex }

class Graph where
  --  getSiblingVertex :: Graph -> Vertex -> [Vertex]
	